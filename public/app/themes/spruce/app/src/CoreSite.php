<?php

use Timber\Site as TimberSite;

class CoreSite extends TimberSite {

	protected $menus = [
		'header_menu' => 'Header Menu',
		'social_menu' => 'Social Menu',
		'footer_menu' => 'Footer Menu',
		'home_menu' => 'Home Menu',
	];

	protected $settings = [
		'Theme General Settings' => 'Theme Settings',
		'Social Settings' => 'Social Settings',
	];

	public $tplReference;
	protected $customTplReference = "layouts/custom.twig";
	protected $baseTplReference = "layouts/theme.twig";
	protected $useDefaultTheme = true;

	public function __construct() {
		$this->tplReference = $this->useDefaultTheme ? $this->baseTplReference : $this->customTplReference;
		
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_theme_support( 'custom-logo', array(
			'height'      => 100,
			'width'       => 400,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		));

		add_action( 'widgets_init', array($this, 'register_widgets') );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_filter('the_generator', function() {} );
		add_filter('show_admin_bar', '__return_false');

		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', function() {
			if (current_user_can('administrator')) 
			{
				wp_enqueue_style('admin-ff-styles', get_template_directory_uri() . '/app/static/styles.css');
			}
		} );
		add_action( 'init', array( $this, 'polylang_register_strings' ) );
		add_action( 'init', array( $this, 'add_options_page' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'admin_menu', array( $this, 'remove_menus' ) );
		add_action( 'init', array( $this, 'disable_emojis' ));

		add_filter( 'wpseo_metabox_prio', function() { return 'low'; } );

		add_action('admin_head', array( $this, 'admin_styles' ));

		remove_action('wp_head', 'wp_generator');
		$this->add_menu_location();
		parent::__construct();
	}

	public function admin_styles() {
		// wp_enqueue_style('admin-ff-styles', get_template_directory_uri() . '/static/css/admin.css');
	}

	public function polylang_register_strings() {
		if (function_exists('pll_the_languages')) {
			global $pagenow;
			if (is_admin() && $pagenow === 'admin.php' && isset($_GET['page'])) {
				if ($_GET['page'] === 'mlang_strings') {
					$helper = new PolylangTranslationRegister();
					$helper->run();
				}
			}
		}
	}

	public function register_widgets() {
		
	}

	public function add_options_page() {
		if( function_exists('acf_add_options_page') ) {
		 	foreach ($this->settings as $settingTitlePage =>  $settingMenuTitle):
		 		acf_add_options_sub_page(array(
					'page_title' 	=> $settingTitlePage,
					'menu_title' 	=> $settingMenuTitle,
					'parent_slug' 	=> "options-general.php",
				));
		 	endforeach;
		}
	}

	public function remove_menus(){
		// remove_menu_page( 'index.php' );                  //Dashboard
		remove_menu_page( 'jetpack' );                    //Jetpack* 
		// remove_menu_page( 'edit.php' );                   //Posts
		// remove_menu_page( 'upload.php' );                 //Media
		// remove_menu_page( 'edit.php?post_type=page' );    //Pages
		remove_menu_page( 'edit-comments.php' );          //Comments
		// remove_menu_page( 'themes.php' );                 //Appearance
		// remove_menu_page( 'plugins.php' );                //Plugins
		// remove_menu_page( 'users.php' );                  //Users
		// remove_menu_page( 'tools.php' );                  //Tools
		// remove_menu_page( 'options-general.php' );        //Settings
		// remove_menu_page( 'edit.php?post_type=acf-field-group' );        //ACF
	}

	function add_menu_location() {
		// add header_menu to Wordpress
		$menus = $this->menus;
		$tr = function_exists('pll_the_languages') ? "pll__" : "__";
		add_action('init', function() use ($tr, $menus) {
			foreach ($menus as $key => $menu):
				register_nav_menu($key, $tr( $menu ));
			endforeach;
		});

		// add header_menu to Timber Context
		add_filter('timber_context', function($data) use ($menus) {
			foreach ($menus as $key => $menu):
				$data[$key] = new Timber\Menu($key);
			endforeach;
			//
			return $data;
		});
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['menu'] = new Timber\Menu();
		$context['site'] = $this;
		if (function_exists("get_fields")) {
			if (get_fields("options") !== null)
				$context['options'] = get_fields('options');
		}

		$r = new Request();
		$context["realpath"] =  $r->getScheme() . "://" . $r->getHost() . $r->getUri();

		return $context;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		// $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		$twig->addFunction(new Twig_SimpleFunction('s', function() {
			// 
			print "<pre>";
			try {
				throw new Exception();
			} catch (Exception $e) {
				if (php_sapi_name() !== "cli") {
					header('Content-Type: text/html; charset=utf-8');
				}
				$trace = $e->getTrace();
				if (isset($trace[0])) {
					$file = realpath($trace[0]["file"]);
					$line = $trace[0]["line"];
					$where = sprintf("", $file, $line);
					$sep1 = str_repeat("─", mb_strlen($file) + 2)."┬".str_repeat("─", mb_strlen($line) + 2);
					$sep2 = str_repeat("─", mb_strlen($file) + 2)."┴".str_repeat("─", mb_strlen($line) + 2);
					printf("┌%s┐\n│ %s │ %d │\n└%s┘\n\n", $sep1, $file, $line, $sep2);
				}
			}
			foreach (func_get_args() as $value) {
				debug_zval_dump($value);
				print PHP_EOL;
			}
			print "</pre>";

			// exit(0);
		}));

		$twig->addFunction(new Twig_SimpleFunction('shuffle', function ($array) {
			shuffle($array);
			return $array;
		}));

		$twig->addFunction(new Twig_SimpleFunction('setUri', function($node,$variable) {
			$get = $_GET;
			$get[$node] = $variable;
			$str = [];
			foreach ($get as $id => $name) {
				if ($id == $node)
					continue;
			    $str[] = sprintf("%s=%s",$id,$name);
			}
			//
			$str[] = sprintf("%s=%s",$node,$variable);

			return parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH) . "?" . implode("&", $str);
		}));

		$twig->addFunction(new Twig_SimpleFunction('hasInUri', function($node,$variable) {
			return isset($_GET[$node]) && $_GET[$node] == $variable;
		}));

		$twig->addFunction(new Twig_SimpleFunction('get_field', function($fieldName, $id) {
			return get_field($fieldName, $id);
		}));

		$twig->addFunction(new Twig_SimpleFunction('pll__', function( $text ) {
				return function_exists('pll__')? pll__($text) : __($text);
		}));

		$twig->addFunction(new \Twig_SimpleFunction('pll_e', function( $text ) {
			return function_exists('pll_e') ? pll_e($text) : $text;
		} ));

		return $twig;
	}

		/**
	 * Disable the emoji's
	 */
	public function disable_emojis() {
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		add_filter( 'tiny_mce_plugins', array( $this, 'disable_emojis_tinymce') );
		add_filter( 'wp_resource_hints', array( $this, 'disable_emojis_remove_dns_prefetch'), 10, 2 );
	}

	/**
	 * Filter function used to remove the tinymce emoji plugin.
	 * 
	 * @param array $plugins 
	 * @return array Difference betwen the two arrays
	 */
	public function disable_emojis_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}

	/**
	 * Remove emoji CDN hostname from DNS prefetching hints.
	 *
	 * @param array $urls URLs to print for resource hints.
	 * @param string $relation_type The relation type the URLs are printed for.
	 * @return array Difference betwen the two arrays.
	 */
	public function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
		if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
			$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

			$urls = array_diff( $urls, array( $emoji_svg_url ) );
		}

		return $urls;
	}

}
