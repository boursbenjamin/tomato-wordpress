<?php
/**
 * Template Name: Home
 * Description: The custom homepage template
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber\Timber::get_context();
$post = Timber\Timber::query_post();

$context['post'] = $post;

Timber\Timber::render( 'custom/pages/home.html.twig', $context );
