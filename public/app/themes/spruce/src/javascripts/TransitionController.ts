import "gsap/ScrollToPlugin";
import { TimelineLite, TweenLite, Power3, Power2 } from "gsap";
import * as $ from "jquery";
import PageTransitionManager from "./PageTransitionManager";

export class TransitionController {

    public static transitionMenu(content, initNewContent, isBack) {
        
        let previousS, newS;
        if (isBack) {
            previousS = content.newS;
            newS = content.previousS;
        } else {
            previousS = content.previousS;
            newS = content.newS;
        }

        const slideActive = $(".js_slide.active");
        const timelineMenu = new TimelineLite({
            paused: true,
            onComplete: () => {
                previousS.remove();
                PageTransitionManager.isAnimating = false;
                PageTransitionManager.body.removeClass("transition");
                newS.removeClass("notVisible");
                initNewContent();
            },
            onReverseComplete: () => {
                newS.remove();
                PageTransitionManager.isAnimating = false;
                PageTransitionManager.body.removeClass("transition");
                newS.removeClass("notVisible");
                initNewContent();
            }
        });

        timelineMenu.fromTo(slideActive, 0.6, {
            transform: "scale(1)",
        }, {
                transform: "scale(1.2)",
                ease: Power2.easeOut,
            });

        timelineMenu.fromTo(newS, 1, {
            top: "100%",
        }, {
                top: "0%",
                ease: Power3.easeOut,
            });

        timelineMenu.fromTo(previousS, 1, {
            top: "0%",
        }, {
            top: "-100%",
            ease: Power3.easeOut,
        }, "-=1");

        if (isBack) {
            timelineMenu.reverse(0);            
        } else {
            timelineMenu.play();
        }

    }

    public static transitionArticle(content, initNewContent, isBack) {

        console.log(content);

        let previousS, newS;
        if (isBack) {
            previousS = content.newS;
            newS = content.previousS;
        } else {
            previousS = content.previousS;
            newS = content.newS;
        }

        previousS.addClass("fadeOut");
        const imagePrev = $("li.article.clicked img", previousS);
        const bboxImagePrev = imagePrev.get(0).getBoundingClientRect();
        console.log(imagePrev);
        console.log(bboxImagePrev);

        const imageNew = $("#mainImage", newS);
        const bboxImageNew = imageNew.get(0).getBoundingClientRect();
        
        // console.log(imageNew);
        // console.log(bboxImageNew);
        
        const translateValue = {
            x: bboxImageNew.left - bboxImagePrev.left,
            y: bboxImageNew.top - bboxImagePrev.top,
        }
            
        // console.log(translateValue);

        const timelineMenu = new TimelineLite({
            onComplete: () => {
                previousS.remove();
                PageTransitionManager.isAnimating = false;
                PageTransitionManager.body.removeClass("transition");
            }
        });

        timelineMenu.to(window, 0.6, {
            ease: Power2.easeOut,
            scrollTo: {
                y: 0
            },
        });

        timelineMenu.to(imagePrev, 0.6, {
            width: bboxImageNew.width,
            height: bboxImageNew.height,
            ease: Power2.easeOut,
            transform: `translateX(${translateValue.x}px) translateY(${translateValue.y}px)`,
        }, "-= 0.6");

        newS.css({
            opacity: 0
        });
        newS.removeClass("notVisible");

        timelineMenu.fromTo(newS, 0.6, {
            opacity: 0
        }, {
            delay: 0.2,
            ease: Power2.easeOut,
            opacity: 1
        });

        timelineMenu.fromTo(previousS, 0.6, {
            opacity: 1
        }, {
            delay: 0.2,
            ease: Power2.easeOut,
            opacity: 0
        }, "-=0.6");

        // timelineMenu.fromTo(previousS);

    }

}