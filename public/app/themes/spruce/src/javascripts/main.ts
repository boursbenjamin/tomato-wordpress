import "../styles/main.scss";
import PageTransitionManager from "./PageTransitionManager";

const main: HTMLElement = document.querySelector(".main-content");
const pageName = main.classList.toString().match(/(page-.*)\s/)[1];

const app = new PageTransitionManager(pageName);