import { lory, LoryStatic } from "lory.js";
import PageTransitionManager, { ChangePage } from "./PageTransitionManager";
import * as $ from "jquery";

export default class Menu {

    private colors = [
        "aqua",
        "bisque",
        "beige",
        "coral",
    ];


    private containerSlider: HTMLElement = document.querySelector('.menu-slider');
    private slides: NodeListOf<Element> = document.querySelectorAll(".js_slide");
    private dots: NodeListOf<Element> = document.querySelectorAll(".dots li");
    private lory: LoryStatic;
    private changePage: ChangePage;

    public static indexSlideClicked: number;

    public constructor(changePage: ChangePage) {
        this.changePage = changePage;
        this.containerSlider.addEventListener("before.lory.init", () => {
            this.initDots();
        });
        this.containerSlider.addEventListener('after.lory.slide', this.handleAfterLorySlide);
        this.lory = lory(this.containerSlider);

        for (let i = 0; i < this.slides.length; i++) {
            const element = this.slides[i];
            element.addEventListener("click", this.handleClickSlide.bind(this, i));
        }

        if (Menu.indexSlideClicked) {
            this.lory.slideTo(Menu.indexSlideClicked);
        }
    }

    private initDots = () => {
        for (let i = 0; i < this.dots.length; i++) {
            this.dots[i].addEventListener("click", () => {
                this.lory.slideTo(i);
            });
            
        }
        this.dots[0].classList.add("active");
    }

    private updateDots = (index: number) => {
        for (let i = 0; i < this.dots.length; i++) {
            this.dots[i].classList.remove("active");
        }
        this.dots[index].classList.add("active");
    }

    private handleClickSlide = (index: number, event) => {
        event.preventDefault();

        const isActive: boolean = event.currentTarget.classList.contains("active");
        if (isActive) {
            const link = event.currentTarget.querySelector("a");
            const urlNewPage = $(link).attr("href");
            const transition = $(link).attr("data-type");

            Menu.indexSlideClicked = index;

            if (urlNewPage === window.location.pathname) {
                console.log("you are going to the same page");
                return;
            }

            if (!PageTransitionManager.isAnimating) {
                this.containerSlider.classList.add("noTransitionChild");
                this.changePage(urlNewPage, transition);
            }

        } else {
            this.lory.slideTo(index);
        }
    }

    private handleAfterLorySlide = (event) => {
        const currentSlide = event.detail.currentSlide;
        this.updateDots(currentSlide);
        this.containerSlider.style.background = this.colors[currentSlide];
    }

}